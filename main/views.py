from django.db.models.query_utils import Q
from django.http import request
from django.http.response import HttpResponse, HttpResponseNotAllowed, JsonResponse
from django.shortcuts import redirect, render
from django.contrib import auth
from main.models import CustomUser, Pengguna
import datetime
from django.views.decorators.csrf import csrf_exempt
import json


# Create your views here.

def login(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = auth.authenticate(request, username=username, password=password)

        if user is not None:
            auth.login(request, user)

            # jika admin
            if user.is_staff:
                return redirect('/admin-dashboard')
            
            # jika bukan admin
            else:
                return redirect("/home")
        
        else:
            context = {
                'error' : 'Kredensial yang dimasukkan salah'
            }
            return render(request, 'main/login.html', context)
    
    if request.user.is_authenticated and request.user.is_active:
        return redirect("/home")
    
    return render(request, 'main/login.html')



def logout(request):
    auth.logout(request)
    return redirect("/")


def register(request):
    if request.method == "POST":
        register_type = request.POST["register_type"]
        nama_belakang = request.POST["nama_belakang"]
        nama_depan = request.POST["nama_depan"]
        username = request.POST["username"]
        password1 = request.POST["password1"]
        password2 = request.POST["password2"]
        alamat = request.POST["alamat"]
        no_telepon = request.POST["no_telepon"]
        email = request.POST["email"]

        if register_type == "pengguna":
            tanggal_lahir = request.POST["tanggal_lahir"]
            jenis_kelamin = request.POST["jenis_kelamin"]
        
        # soon
        # elif register_type == "venue" :
        #     jenis_venue =
            
        try:
            user = CustomUser.objects.get(username=username)
            if user is not None:
                context = {
                    "error":"User dengan username atau email tersebut sudah terdaftar"
                }
                if register_type == "pengguna":
                    return render(request, "main/register-pengguna.html", context)

                # soon
                # return render(request, "main/register-venue.html", context)
        
        except CustomUser.DoesNotExist:
            user = CustomUser(
                username = username,
                first_name = nama_depan,
                last_name = nama_belakang,
                email = email,
                alamat = alamat,
                no_telepon = no_telepon
            )
            user.set_password(password1)
            
            if register_type == "pengguna":
                pengguna = Pengguna()
                user.is_active = True
                user.is_pengguna = True
                # user.is_venue = False
                pengguna.user = user
                pengguna.tanggal_lahir = tanggal_lahir
                pengguna.jenis_kelamin = jenis_kelamin
                user.save()
                pengguna.save()
                user = auth.authenticate(request, username=username, password=password1)
                auth.login(request, user)
                return redirect("/home")

            # soon
            # elif register_type == "venue":
            #     user.is_active = False


    if request.user.is_authenticated and request.user.is_active:
        return redirect("/home")
    
    return render(request, "main/register-home.html")

            
def registrasi_pengguna(request):
    return render(request, "main/registrasi-pengguna.html")

# soon
# def registrasi_venue(request):
#     return blablabla

# soon
# def check_venue(request):
#     nanti