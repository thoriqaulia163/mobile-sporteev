from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.base import ModelState
from django.db.models.enums import Choices
from django_dropbox_storage.storage import DropboxStorage

# Create your models here.

class CustomUser(AbstractUser):
    is_pengguna = models.BooleanField("user status", default=False)
    is_admin = models.BooleanField(default=False)
    alamat = models.TextField(max_length=250)
    no_telepon = models.TextField(max_length=15)
    # soon bakal ada is venue


class Pengguna(models.Model):
    DROPBOX_STORAGE = DropboxStorage()
    user = models.OneToOneField(CustomUser, on_delete = models.CASCADE)
    tanggal_lahir = models.DateField()
    JENIS_KELAMIN = (
        (1, "LAKI-LAKI"),
        (2, "PEREMPUAN"),
    )
    jenis_kelamin = models.IntegerField(choices=JENIS_KELAMIN)
    is_active = True
    photo = models.ImageField(upload_to='photos', storage=DROPBOX_STORAGE)
    level = models.IntegerField(default=1)
    total_exp = models.IntegerField(default=0)
    current_exp = models.IntegerField(default=0)

    def __str__(self):
        return str(self.user)

# soon
# class Venue(models.Model):
#     is_active = False

class PhotoTest(models.Model):
    DROPBOX_STORAGE = DropboxStorage()
    photo = models.ImageField(upload_to='photos', storage=DROPBOX_STORAGE, blank=True, null=True, default=None)
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name