from django.contrib import admin
from .models import Event, EventRequest
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(Event)
admin.site.register(EventRequest)