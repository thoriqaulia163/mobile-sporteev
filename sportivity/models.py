from typing import Callable
from django.db import models
from django.db.models.deletion import CASCADE
from main.models import Pengguna
from django_dropbox_storage.storage import DropboxStorage
# from location_field.models.plain import PlainLocationField
from django.db.models.fields.related import ManyToManyField


# Create your models here.

# class Place(models.Model):
#     city = models.CharField(max_length=255)
#     location = PlainLocationField(based_fields=['city'], zoom=7)

class Event(models.Model):
    DROPBOX_STORAGE = DropboxStorage()
    nama_event = models.CharField(max_length=50)
    host = models.ForeignKey(Pengguna, on_delete=CASCADE, blank=False)
    tanggal = models.DateField(auto_now=False, auto_now_add=False)
    waktu_start = models.TimeField(auto_now=False, auto_now_add=False)
    waktu_end = models.TimeField(auto_now=False, auto_now_add=False)
    # venue = models.ForeignKey(Venue, on_delete=CASCADE, blank=False)
    venue = models.CharField(max_length=50, blank=False)
    alamat = models.CharField(max_length=100, blank=False)
    harga = models.IntegerField(default=0)
    JENIS_OLAHRAGA = (
        (1, "Badminton"),
        (2, "Sepak bola"),
        (3, "Basket"),
        (4, "Voli"),
        (5, "Tenis"),
        (6, "Tenis Meja"),
        (7, "Baseball"),
        (8, "Bersepeda"),
        (9, "Lari"),
        (10, "Other"),
    )    
    jenis_olahraga = models.IntegerField(choices=JENIS_OLAHRAGA, default=1)
    LEVEL = (
        (0, "All Level"),
        (1, "Rookie"),
        (2, "Beginner"),
        (3, "Intermediete"),
        (4, "Advanced"),
        (5, "Expert"),
    )
    level = models.IntegerField(choices=LEVEL, default=1)
    min_participant = models.IntegerField()
    max_participant = models.IntegerField()
    current_participant = models.IntegerField(default=1)
    deskripsi = models.TextField()
    is_ranked = models.BooleanField(default=False)
    is_private = models.BooleanField(default=False)
    photo = models.ImageField(upload_to='photos', storage=DROPBOX_STORAGE)


class EventRequest(models.Model):
    event_id = models.ForeignKey(Event, on_delete=CASCADE, blank=False)
    participant = models.ForeignKey(Pengguna, on_delete=CASCADE, blank=False)
    STATUS_REQUEST = (
        (0, "Draft"),
        (1, "Menunggu Konfirmasi"),
        (2, "Diterima"),
        (3, "Ditolak")
    )
    status_request = models.IntegerField(choices=STATUS_REQUEST, default=0)
    