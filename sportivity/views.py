from django.shortcuts import redirect, render
from main.models import Pengguna
from django.views.decorators.csrf import csrf_exempt
from sportivity.models import Event, EventRequest

# Create your views here.

def home_sportivity(request):
    list_event = list(Event.objects.filter(is_private=False))
    context = {
        'list_event' : list_event
    }
    print(list_event)
    return render(request, 'sportivity/list_event.html', context)

def buat_event_jenis(request):
    if request.method == "POST":
        jenis_olahraga = request.POST["jenis_olahraga"]
        event = Event.objects.create(
            jenis_olahraga = jenis_olahraga
        )
    return redirect("/home")

def buat_event_jenis(request):
    if request.method == "POST":
        jenis_olahraga = request.POST["jenis_olahraga"]
        event = Event.objects.create(
            jenis_olahraga = jenis_olahraga
        )
    return redirect("/home")