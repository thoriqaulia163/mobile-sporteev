from django.shortcuts import render, redirect
from main.models import Pengguna
from django.contrib import auth
# from userprofile.models import Kendaraan
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
import datetime
import string
import random


# Create your views here.

# Global Varieble
list_olahraga = [
            {'val':1,'olahraga':'Futsal'},
            {'val':2,'olahraga':'Basket'},
            {'val':3,'olahraga':'Badminton'},
            {'val':4,'olahraga':'Voli'},
            {'val':5,'olahraga':'Tenis'},
            {'val':6,'olahraga':'Tenis Meja'},
            {'val':7,'olahraga':'Baseball'},
            {'val':8,'olahraga':'Sepak Bola'},
            {'val':8,'olahraga':'Lari'},
            {'val':8,'olahraga':'Berenang'},
        ]

lst_hari = [
            {'val':1,'hari': "Senin"},
            {'val':2,'hari': "Selasa"},
            {'val':3,'hari': "Rabu"},
            {'val':4,'hari': "Kamis"},
            {'val':5,'hari': "Jumat"},
            {'val':6,'hari': "Sabtu"},
            {'val':7,'hari': "Minggu"}
        ]


alpha_numeric = string.ascii_uppercase + string.digits
#generate random string
def id_generator(size=6, chars=alpha_numeric):
    return ''.join(random.choice(chars) for _ in range(size))


@login_required(login_url="/")
def profil(request):
    #soon
    # if request.user.is_venue: 
    #     return redirect("/home")

    if request.user.is_staff:
        return redirect("/admin")

    else :
        user = request.user
        pengguna = Pengguna.objects.get(user=request.user)
        foto = ""
        print(foto)
        try:
            foto = pengguna.photo.url
        except:
            foto = False

        context = {
            'foto': foto,
            'nama_depan': user.first_name,
            'nama_belakang': user.last_name,
            'username': user.username,
        }
        return render(request, "profil/profil.html", context)

@csrf_exempt
@login_required(login_url="/")
def edit_profil(request):
    #soon
    # if request.user.is_venue: 
    #     return redirect("/home")

    if request.user.is_staff:
        return redirect("/admin")

    else :
        if request.method == "POST":
            user = request.user
            pengguna = Pengguna.objects.get(user=request.user)

            nama_depan = request.POST["nama_depan"]
            nama_belakang = request.POST["nama_belakang"]
            alamat = request.POST["alamat"]
            email = request.POST["email"]
            tanggal_lahir = request.POST["tanggal_lahir"]
            jenis_kelamin = request.POST["jenis_kelamin"]
            password = request.POST["password1"]
            username = request.POST["username"]
            no_telepon = request.POST["no_telepon"]

            user.alamat = alamat
            user.first_name = nama_depan
            user.last_name = nama_belakang
            user.email = email
            user.no_telepon = no_telepon

            password_is_changed = False
            if password != '':
                password_is_changed = True
                user.set_password(password)
            user.save()

            if request.FILES.get('myfile',False):
                new_foto = request.FILES['myfile']

                foto_format = "." + ((new_foto.name).split("."))[-1]
                new_foto_name = (new_foto.name).replace(foto_format,"")
                if pengguna.photo.name != "":
                    prev_foto_name = ((pengguna.photo.name).split(".")[0])[15:]
                else:
                    prev_foto_name = alpha_numeric
                # (petunjuk) new foto name <-- rand(new_foto) + rand(prev_foto) + rand(string) + file formats
                new_foto.name = id_generator(6,new_foto_name) + id_generator(6,prev_foto_name) + id_generator() + foto_format

                pengguna.photo = new_foto
                
            pengguna.tanggal_lahir = tanggal_lahir
            pengguna.jenis_kelamin = jenis_kelamin
            pengguna.save()

            if password_is_changed:
                new_login = auth.authenticate(request, username=username, password=password)
                auth.login(request, new_login)
            
            return redirect("/profil")

        else:
            user = request.user
            pengguna = Pengguna.objects.get(user=request.user)

            foto = ""
            try:
                foto = pengguna.photo.url
            except:
                foto = False

            context = {
                'nama_depan':user.first_name,
                'nama_belakang':user.last_name,
                'username':user.username,
                'no_telepon':user.no_telepon,
                'email':user.email,
                'tanggal_lahir':str(pengguna.tanggal_lahir),
                'jenis_kelamin':pengguna.jenis_kelamin,
                'foto':foto,
            }

            return render(request, "profil/edit-profil.html", context)

