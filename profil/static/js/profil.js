
$("#display-frame").click(function() {
    $("input[id='imageFileInput']").click();
  });
  
  
  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
  
        reader.onload = function (e) {
            $('#display-picture')
                .attr('src', e.target.result);
        };
  
        reader.readAsDataURL(input.files[0]);
    }
  }
  
  $(document).ready(function() {
  
    $("#jenis").change(function() {
        var val = $(this).val();
        $("#merek").html(options[val]);
    });
  
  
    var options = [
        "",
        `
        <option value="1">Honda</option>
        <option value="2">Toyota</option>
        <option value="3">Mitsubishi</option>
        <option value="4">Nissan</option>
        <option value="5">Daihatsu</option>
        <option value="6">BMW</option>
        `,
        `
        <option value="1">Honda</option>
        <option value="2">Yamaha</option>
        <option value="3">Suzuki</option>
        <option value="4">Kawasaki</option>
        <option value="5">Ducati</option>
        <option value="6">KTM</option>
          `
    ];
  
  });